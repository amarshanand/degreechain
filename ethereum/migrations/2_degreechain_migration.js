/**
 * This migration script is our 'infrastructure-setup'.
 * It reads the SmartContracts (*.sol files), and deploys them to the network that is mentioned in package.json's deploy script.
 * All this is performed by the Admin user (identified by the ADMIN_PRIVATE_KEY in .env file)
 */

const DegreeLedger = artifacts.require("./DegreeLedger.sol");

module.exports = function (deployer) {
  deployer.deploy(DegreeLedger).then(function () {});
};
