var HDWalletProvider = require("truffle-hdwallet-provider-privkey");
require("dotenv").config();

if (!process.env.ADMIN_PRIVATE_KEY || !process.env.INFURA_PROJECT_ID)
  throw new Error(
    "FATAL: Either the .env file is missing, or it doesnt have ADMIN_PRIVATE_KEY and INFURA_PROJECT_ID fields. Please refer to Readme for the fix"
  );

module.exports = {
  compilers: {
    solc: {
      version: "0.8.4",
    },
  },
  networks: {
    development: {
      host: "127.0.0.1",
      port: 7545,
      network_id: "*",
    },
    ropsten: {
      provider: () => new HDWalletProvider([process.env.ADMIN_PRIVATE_KEY], `https://ropsten.infura.io/v3/${process.env.INFURA_PROJECT_ID}`),
      network_id: 3,
      gas: 4000000, //make sure this gas allocation isn't over 4M, which is the max
    },
  },
};
