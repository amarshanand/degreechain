# Introduction

This is where I talk to myself, and an imaginary dev who is taking handover from me. I will write sufficient comments in the code, but things like shortcuts, gotchas etc is what I will put here. The page is quite randomly organised, more like a scratchpad.

## Getting a basic SmartContracts to work

### Configuration .env files

Before attempting to run this app locally, you need to produce an `.env` file in the `/ethereum` folder:

```
# /ethereum/.env

ADMIN_PRIVATE_KEY = 'e66b5...797a5'
INFURA_PROJECT_ID = 'fb69a665...cebe406a9'
```

### Obtaining ADMIN_PRIVATE_KEY

1. Install [Metamask Chrome Extension](https://metamask.io/).
2. Create a Metamask account. This will autometically create one default account for you. We would use this as our `Admin` account.
3. In Metamask, look for `Account Details` of this account.
4. In `Account Details`, rename this account as `Admin`, and click on `Export Private Key`.
5. Reveal and Copy this `Private Key` to the `ADMIN_PRIVATE_KEY` field in `/ethereum/.env`

The `Admin` account will be used to upload (instantiate) SmartContracts to Ethereum.

### Obtaining INFURA_PROJECT_ID

1. Create an account on [Infura](https://infura.io/)
2. Create a new Project
3. Copy the `PROJECT ID` of this new Project and paste it in the `INFURA_PROJECT_ID` field of the `/ethereum/.env` file

`Infura` provides an API access to Ethereum. Consider it as GrapQL / Postgres combo - GraphQL provides a neat API layer on top of the ugly SQL layer that comes as a default from Postgres.

## Deploying SmartContracts to Ethereum

We are deploying our SmartContracts to [Ropsten Testnet](https://ropsten.etherscan.io/).

The `ADMIN_PRIVATE_KEY` in the `/ethereum/.env` tells the deployer script of which account to use for deploying the SmartContracts. To put funds into the `Admin` account, go to [MetaMask Ether Faucet](https://faucet.metamask.io/).

Click on `request 1 ether from faucet` button. If is down or errored, you can use [Ropsten Ethereum Faucet](https://faucet.ropsten.be/) or similar.

Once you have sufficient funds (about 2Eth is enough), run the following from `/ethereum` folder:

```
% npm install
% npm run deploy
```

If all goes well, you will see something similar to `contract address: 0x509d...F363` (one for each SmartContract), which is where the SmartContract gets deployed. [You can view and interact with the SmartContract in Remix](https://remix-ide.readthedocs.io/en/latest/run.html#deploy-ataddress).

### Linking files to /src folder

React expects all files required by the App to be within its `/src` folder. Hence, we have linked the folder `/ethereum/build/contracts -> /admin/src/blockchain/contracts`. If you seen errors similar to `Module not found: Can't resolve './contracts' in ...`, perform the linking as follows:

```
# from admin/src/blockchain dir, link /contracts folder
% rm -rf ./contracts
% ln -s ../../../ethereum/build/contracts ./contracts

# also, link the solidity files
% rm -rf ./contracts/src
% ln -s ../../../ethereum/contracts ./contracts/src
```
