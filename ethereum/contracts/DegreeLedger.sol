// SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.4;

/**
 * The DegreeLedger allows to add a degree to Ethereum and emit an event susequently
 */

contract DegreeLedger {
    
    // the owner of the contract
    address payable owner;

    // a hashtable of degree's ipfsHash against timestamp
    mapping(string => uint) public degreeTable;

    // event to broadcast that a new degree is being added
    event DegreeAdded(address from, string ipfsHash, string studentId, string studentName, uint timestamp);

    // constructor
    constructor ()  {
        owner = payable(msg.sender);
    }
    
    /**
     * To add a Degree, we:
     * 1. Ensure that ipfsHash doesnt exists already
     * 2. Add the hash to ipfsHashesHT
     * 3. Emit an event, boradcasting the creation of the new degree
     */
    function addDegree(string memory _ipfsHash, string memory _studentId, string memory _studentName) public {
        require(msg.sender != owner, "the owner of this contract can never add a degree");
        require(degreeTable[_ipfsHash] == 0, "this degree already exists");
        degreeTable[_ipfsHash] = block.timestamp;
        emit DegreeAdded(msg.sender, _ipfsHash, _studentId, _studentName, block.timestamp);
    }

    // enusre that no one transfers anything to this account
    receive() payable external { revert("this contract cannot accept any ethers"); }
    
}
