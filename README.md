[![Degreechain - Complete walthrough](./video-thumbnail.png)](https://www.youtube.com/watch?v=5c0UWMDQStc)

# About Degreechain

Degreechain is a _standalone_ React App that interacts with Ethereum Blockchain. It has no server component. 

## Running Degreechain locally

To run Degreechain locally, simple pull the project and issue the following:

```
% cd client
% npm install
% npm run start
```
You'd require _Metamask_ (set it to Ropsten network) to use it.

## SmartContract Development

`/ethereum/contracts` folder contains the SmartContract for Degreechain. You can compile and deploy the SmartContrcats using the following:

```
% cd ethereum
% npm install   # make sure you use Node version 10.x
% npm run deploy
```
## Live Demo

[https://amarshanand.gitlab.io/degreechain](https://amarshanand.gitlab.io/degreechain)

You'd require _Metamask_ (set it to Ropsten network) to use it.

## Presentation

[Degreechain Powerpoint Presentation](https://docs.google.com/presentation/d/1IMLXFMzvuMtEfeERUQxei48fhTUe3BiqON-qVnnVZuk/edit?usp=sharing)

