/**
 * Shuffles array in place. ES6 version
 * @param {Array} a items An array containing the items.
 */
const shuffle = (a) => {
  for (let i = a.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [a[i], a[j]] = [a[j], a[i]];
  }
  return a;
};

const shortenHex = (hex) => `${hex.slice(0, 6)}...${hex.slice(-4)}`;

const capitalize = (string) => `${string.charAt(0).toUpperCase()}${string.slice(1)}`;

export { shuffle, shortenHex, capitalize };
