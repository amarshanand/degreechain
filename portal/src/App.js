import "./App.css";
import { MuiThemeProvider } from "@material-ui/core/styles";
import Navigation from "./components/Navigation";
import LandingPage from "./components/LandingPage";
import UploadDocument from "./components/UploadDocument";
import Error from "./components/Error";
import Notification from "./components/Notification";
import Confirm from "./components/Confirm";
import BlockchainLoader from "./blockchain/BlockchainLoader";
import theme from "./theme";
import { useGlobalState } from "./store";

function App() {
  const [crashMessage, setCrashMessage] = useGlobalState("crashMessage");
  const [showUploadDocument] = useGlobalState("showUploadDocument");
  if (crashMessage) return <Error />;
  // check some vital preconditions before launching the app
  if (!process.env.REACT_APP_ETHERSCAN_API_KEY)
    setCrashMessage("Either .env file is not present , or it doesnt have an entry for REACT_APP_ETHERSCAN_API_KEY");
  return (
    <MuiThemeProvider theme={theme}>
      <div className="App">
        <Navigation />
        {showUploadDocument ? <UploadDocument /> : <LandingPage />}
        <Notification />
        <Confirm />
        <BlockchainLoader />
      </div>
    </MuiThemeProvider>
  );
}

export default App;
