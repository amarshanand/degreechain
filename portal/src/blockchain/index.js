// @ts-nocheck

/**
 * This service is our proxy for Ethereum SmartContracts. This is the only file that should ever access ethereum.
 * In future, if we need to move to flow, we can change these call here and leave the rest of the UI untouhed
 */

import Web3 from "web3";

let web3, setCrashMessage, setNotification, setDegreeLedger, degreeLedgerContract, _degreeLedger;

/**
 * To be called at starting of the App. This method reads all the events since the beginning of time, and sets the state
 * in the Store. All further events are applied to the Store to alter the state of the system incrementally.
 */
const initBlockchain = async (_setCrashMessage, _setNotification, _setBlockchainReady, _setDegreeLedger) => {
  setCrashMessage = _setCrashMessage;
  setNotification = _setNotification;
  setDegreeLedger = _setDegreeLedger;
  // we assume that we are using Metamask, and Metamask injects window.ethereum
  if (window.ethereum) await window.ethereum.enable();
  web3 = new Web3(window.ethereum);
  if ((await validateMetamask()) === false) return null;
  // if the user is not logged-in with Metamask, detect it now
  const accounts = await web3.eth.getAccounts();
  if (!accounts) return setNotification({ text: `Login failed. Are you logged-in to Metamask?`, type: "error" });
  web3.eth.defaultAccount = accounts[0];
  // subscribe to accout changed events to reset the defaultAccount
  window.ethereum.on("accountsChanged", (accounts) => (web3.eth.defaultAccount = accounts[0]));
  // setup our DegreeLedger Contract instance
  const { abi, networks } = require(`./contracts/DegreeLedger`);
  degreeLedgerContract = new web3.eth.Contract(abi, networks[await web3.eth.net.getId()].address);
  // collect all past events
  await processDegreeLedgerEvents();
  // if all is well, flag it and move ahead
  _setBlockchainReady(true);
  setNotification({ text: `Connected to Ethereum through Metamask`, type: "success" });
};

/**
 * Validate a bunch of things about Metamask, including whether it is installed or not, and whether Ropsten Test Network is selected.
 * It returns false is web3 is missing, and then ensures that network is set to Ropsten (value '3'). There are situations when web3.version.network
 * comes out to be null, in which case, we keep on reading it until we get a non-null value
 */
const validateMetamask = async () =>
  new Promise(async (resolve, reject) => {
    // if web3 is not detected, the user hasnt perhaps installed Metamask
    if (window.ethereum === null || !window.ethereum || typeof window.ethereum === "undefined" || !window.ethereum.isMetaMask) {
      setCrashMessage(
        'Metamask is not installed or is disabled. Please visit the page after installing Metamask. <br/><br/><a href="https://en.bitcoinwiki.org/wiki/Metamask" target="_blank" class="whitelink">Click here to read more about Metamask and why is it needed</a><br/><br/><a href="https://metamask.io/" target="_blank" class="whitelink">Click here to install Metamask</a>'
      );
      return reject(false);
    }
    resolve(true);
  });

/**
 * Get the balance in finney for the given wallet address.
 * 1 eth = 1000 finney
 * @param {*} address
 */
const getBalance = async (address) => {
  if (address === 0x0 || (await validateMetamask()) === false) return;
  return parseInt(web3.utils.fromWei(await web3.eth.getBalance(`${address}`), "finney"));
};

// process events that gotten emitted while a new Degree was added
const _processDegreeAddedEvent = async ({ transactionHash, returnValues: { from, ipfsHash, studentId, studentName, timestamp } }) =>
  _degreeLedger.unshift({ transactionHash, from, ipfsHash, studentId, studentName, timestamp });

/**
 * Obtain a list of all past events and construct the current state of the system.
 * Events are being sent twice for some reason. So, we maintain a hashtable of events,
 * to ensure that we dont process them twice
 */
const eventTransactionHashTable = {};
const processDegreeLedgerEvents = async () => {
  // first process past events
  _degreeLedger = [];
  const pastEvents = await degreeLedgerContract.getPastEvents({ fromBlock: 0, toBlock: "latest" });
  for (const event of pastEvents) if (event.event === "DegreeAdded") await _processDegreeAddedEvent(event);
  setDegreeLedger(_degreeLedger);
  // next, listen to new events
  const validateEvent = (error, event, alias) => {
    if (Boolean(eventTransactionHashTable[event.transactionHash + event.returnValues.ipfsHash])) return false;
    eventTransactionHashTable[event.transactionHash + event.returnValues.ipfsHash] = true;
    if (!error) return true;
    setNotification({ text: `Error in ${alias} event : ${error}`, type: "error" });
    return false;
  };
  degreeLedgerContract.events.DegreeAdded({ fromBlock: "latest" }, async (error, event) => {
    if (!validateEvent(error, event, "DegreeAdded")) return;
    await _processDegreeAddedEvent(event);
    setNotification({ text: `Degree added successfully`, type: "success" });
  });
};

/**
 * Add an degree
 */
const addDegree = async (ipfsHash, studentId, studentName) =>
  new Promise(function (resolve, reject) {
    doTransact(degreeLedgerContract.methods.addDegree, [ipfsHash, studentId, studentName], null, "Adding Degree", (receipt) => {
      if (!receipt.status) {
        setNotification({ text: `Error while Adding Degree`, type: "error" });
        return reject(false);
      }
      return resolve(`Degree added successfully`);
    });
  });

/**
 * A generic method that executes a txnFunction(). Its does neccessary updates to the UI (locking, notification, etc)
 * @param {*} txnFunction
 * @param {*} args
 * @param {*} caption
 * @param {*} done a callback when the txn has been mined. a true/false value tells whether the transaction was successful
 */
const doTransact = (txnFunction, args, value, caption, done) => {
  const pollTxn = (hash) => {
    web3.eth.getTransactionReceipt(hash, (err, receipt) => {
      if (receipt) return done(receipt);
      setTimeout(() => pollTxn(hash), 1000);
    });
  };
  const callback = (err, txnHash) => {
    if (err) return done(err.message);
    setNotification({ text: `Transaction submitted for ${caption}`, type: "info" });
    pollTxn(txnHash);
  };
  txnFunction.apply(null, args).send({ from: web3.eth.defaultAccount, value: web3.utils.toWei(value || "0", "finney") }, callback);
};

export { initBlockchain, getBalance, addDegree };
