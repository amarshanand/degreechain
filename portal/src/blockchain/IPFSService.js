import { PINATA } from "../config";
const { API_ENDPOINT, API_KEY, SECRET_API_KEY } = PINATA;

/**
 * This service is our proxy for IPFS. We use Pinata to save / retrieve our files
 */
let setNotification;

/**
 * To be called at starting of the App. This method reads all the events since the beginning of time, and sets the state
 * in the Store. All further events are applied to the Store to alter the state of the system incrementally.
 */
const initIPFS = async (_setNotification, _setIPFSReady) => {
  setNotification = _setNotification;
  if (!(await pingIPFS())) return setNotification({ text: `Could not connect to IPFS`, type: "error" });
  _setIPFSReady(true);
  setNotification({ text: `Connected to IPFS`, type: "success" });
};

/**
 * Hit the IPFS node to ensure that we can reach it
 */
const pingIPFS = async () => {
  try {
    const { status } = await fetch(`${API_ENDPOINT}/data/testAuthentication`, {
      headers: {
        pinata_api_key: API_KEY,
        pinata_secret_api_key: SECRET_API_KEY,
      },
    });
    return status === 200;
  } catch (e) {
    console.error(e);
    return false;
  }
};

/**
 * Upload the given file to IPFS
 * @param file
 */
const uploadToIPFS = async (file, metadata) => {
  try {
    const data = new FormData();
    data.append("file", file);
    const _metadata = JSON.stringify({
      name: metadata.fileName,
      keyvalues: metadata,
    });
    data.append("pinataMetadata", _metadata);
    const { IpfsHash } = await (
      await fetch(`${API_ENDPOINT}/pinning/pinFileToIPFS`, {
        method: "post",
        body: data,
        headers: {
          pinata_api_key: API_KEY,
          pinata_secret_api_key: SECRET_API_KEY,
        },
      })
    ).json();
    if (!IpfsHash) throw Error();
    setNotification({ type: "success", text: `File uploaded to IPFS` });
    return IpfsHash;
  } catch (e) {
    setNotification({ type: "error", text: "File could not be uploaded to IPFS" });
    console.error(`Failed to upload file : ${e}`);
  }
};

export { initIPFS, uploadToIPFS };
