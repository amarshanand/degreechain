/**
 * A tiny component that apears only until Blockchain rleated classes are initialised. I had to build this because the
 * we need a way to access GlobalState, which can be accessed only from within a React component.
 */

import { useEffect } from "react";
import { useGlobalState } from "../store";
import { initBlockchain } from "./index.js";
import { initIPFS } from "./IPFSService";

export default function BlockchainLoader() {
  const [, setCrashMessage] = useGlobalState("crashMessage");
  const [, setNotification] = useGlobalState("notification");
  const [, setBlockchainReady] = useGlobalState("blockchainReady");
  const [, setIPFSReady] = useGlobalState("IPFSReady");
  const [, setDegreeLedger] = useGlobalState("degreeLedger");

  useEffect(() => {
    initBlockchain(setCrashMessage, setNotification, setBlockchainReady, setDegreeLedger);
    initIPFS(setNotification, setIPFSReady);
  }, [setCrashMessage, setNotification, setBlockchainReady, setIPFSReady, setDegreeLedger]);

  /* return a blank component */
  return <div />;
}
