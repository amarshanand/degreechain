module.exports = {
  AWS_S3: {
    REGION: process.env.REACT_APP_AWS_REGION,
    IDENTITY_POOL_ID: process.env.REACT_APP_AWS_IDENTITY_POOL_ID,
    BUCKET: "filetrader",
    API_VERSION: "2006-03-01",
  },
  PINATA: {
    API_ENDPOINT: "https://api.pinata.cloud",
    API_KEY: process.env.REACT_APP_PINATA_API_KEY,
    SECRET_API_KEY: process.env.REACT_APP_PINATA_SECRET_API_KEY,
  },
  adminAccount: process.env.REACT_APP_ADMIN_ACCOUNT_ADDRESS,
  etherscanURL: "https://ropsten.etherscan.io",
  etherscanAPI: `https://api-ropsten.etherscan.io/api?module=account&action=txlist&sort=desc&apikey=${process.env.REACT_APP_ETHERSCAN_API_KEY}&address=`,
  ipfsGatewayURL: "https://gateway.pinata.cloud/ipfs",
  noOfAssetsInaAPack: 2,
};
