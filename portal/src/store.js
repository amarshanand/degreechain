import { createGlobalState } from "react-hooks-global-state";

const initialState = {
  blockchainReady: false,
  IPFSReady: false,
  heading: "University Degree Portal",
  confirm: false,
  showUploadDocument: false,
  crashMessage: null,
  notification: null,
  degreeLedger: [],
};

export const { GlobalStateProvider, useGlobalState } = createGlobalState(initialState);
