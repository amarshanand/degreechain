import React, { useState } from "react";
import { withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import TextField from "@material-ui/core/TextField";
import Paper from "@material-ui/core/Paper";
import Icon from "@mdi/react";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import { mdiScriptTextKey, mdiFilePdf, mdiEmailSend, mdiSend } from "@mdi/js";
import { useGlobalState } from "../store";
import { etherscanURL, ipfsGatewayURL } from "../config";

const styles = (theme) => ({
  root: {
    margin: "8rem 4rem",
    border: "1px solid rgba(0,0,0,0.1)",
    width: "calc(100% - 8rem)",
  },
  button: {
    verticalAlign: "top",
    marginLeft: "0.5rem",
  },
  tableRow: {
    "&:nth-of-type(odd)": {
      backgroundColor: "#00000006",
    },
  },
  tableCell: {
    fontWeight: "bold",
    padding: "0.5rem 3rem",
    color: "#333333",
  },
});

function validateEmail(email) {
  const re =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

function DegreeTable(props) {
  const { classes } = props;
  const [showEmailAt, setShowEmailAt] = useState(-1);
  const [email, setEmail] = useState("");
  const [sendingEmail, setSendingEmail] = useState(false);
  const [degreeLedger] = useGlobalState("degreeLedger");
  const [, setNotification] = useGlobalState("notification");

  const emailDegree = async ({ transactionHash, ipfsHash, studentId, studentName }) => {
    try {
      const body = {
        to: email,
        trace: `${etherscanURL}/tx/${transactionHash}`,
        file: `${ipfsGatewayURL}/${ipfsHash}`,
        studentId,
        studentName,
      };
      setSendingEmail(true);
      await fetch("https://degreechain.herokuapp.com/api/sendmail", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(body),
      });
      setShowEmailAt(-1);
      setSendingEmail(false);
      setNotification({ text: `Degree emailed to ${body.to}`, type: "success" });
    } catch (error) {
      setSendingEmail(false);
      setNotification({ text: `Could not email degree`, type: "error" });
    }
  };

  return (
    <Paper className={classes.root} elevation={0}>
      <Table className={classes.table}>
        <TableBody>
          {degreeLedger.map((entry, i) => {
            const { transactionHash, ipfsHash, studentId, studentName } = entry;
            return (
              <TableRow className={classes.tableRow} key={i}>
                <TableCell className={classes.tableCell}>{studentId}</TableCell>
                <TableCell className={classes.tableCell}>{studentName}</TableCell>
                <TableCell className={classes.tableCell} style={{ width: "34rem" }} align="right">
                  {showEmailAt === i && (
                    <div style={{ position: "relative", display: "inline-block", marginRight: "2rem" }}>
                      <TextField
                        style={{ width: "20rem" }}
                        label="Reciepient Email"
                        placeholder="Reciepient Email"
                        variant="outlined"
                        value={email}
                        onChange={(event) => setEmail(event.target.value)}
                        disabled={sendingEmail}
                      />
                      {!sendingEmail && (
                        <IconButton
                          className={classes.button}
                          style={{ position: "absolute", top: "3px", right: "2px" }}
                          onClick={() => emailDegree(entry)}
                          disabled={!validateEmail(email) || sendingEmail}
                        >
                          <Tooltip title={`Send email`}>
                            <Icon path={mdiSend} size={1} />
                          </Tooltip>
                        </IconButton>
                      )}
                      {sendingEmail && (
                        <img style={{ position: "absolute", top: "3px", right: "2px", width: "3rem" }} src="spinner.gif" alt="sending" />
                      )}
                    </div>
                  )}
                  <IconButton className={classes.button} onClick={() => window.open(`${ipfsGatewayURL}/${ipfsHash}`, "_blank")}>
                    <Tooltip title={`View Degree`}>
                      <Icon path={mdiFilePdf} size={1} />
                    </Tooltip>
                  </IconButton>
                  <IconButton className={classes.button} onClick={() => setShowEmailAt(i)}>
                    <Tooltip title={`Email Degree Details`}>
                      <Icon path={mdiEmailSend} size={1} />
                    </Tooltip>
                  </IconButton>
                  <IconButton className={classes.button} onClick={() => window.open(`${etherscanURL}/tx/${transactionHash}`, "_blank")}>
                    <Tooltip title={`View on Blockchain`}>
                      <Icon path={mdiScriptTextKey} size={1} />
                    </Tooltip>
                  </IconButton>
                </TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </Paper>
  );
}

export default withStyles(styles)(DegreeTable);
