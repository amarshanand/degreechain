import { createStyles, makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { useGlobalState } from "../store";

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      top: 0,
      position: "fixed",
      width: "100%",
      zIndex: 1,
    },
    logo: {
      width: "2rem",
    },
    title: {
      flexGrow: 1,
      marginLeft: "-2rem",
    },
    white: {
      color: "white",
      borderColor: "#ffffff99",
      width: "6rem",
    },
  })
);

export default function ButtonAppBar() {
  const classes = useStyles();
  const [heading] = useGlobalState("heading");
  const [showUploadDocument, setShowUploadDocument] = useGlobalState("showUploadDocument");

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <img alt="logo" src="logo.png" className={classes.logo} />
          <Typography variant="h6" className={classes.title}>
            {heading}
          </Typography>
          {!showUploadDocument && (
            <Button variant="outlined" className={classes.white} onClick={() => setShowUploadDocument(true)}>
              Upload
            </Button>
          )}
          {showUploadDocument && (
            <Button variant="outlined" className={classes.white} onClick={() => setShowUploadDocument(false)}>
              Close
            </Button>
          )}
        </Toolbar>
      </AppBar>
    </div>
  );
}
