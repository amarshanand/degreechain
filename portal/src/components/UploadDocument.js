/**
 * This component uploads an document, alongwith its metadata to IPFS and
 * creates an entry in the DegreeLedger (hosted on Ethereum).
 */
import { useState } from "react";
import { createStyles, makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import { DropzoneArea } from "material-ui-dropzone";
import { Document, Page } from "react-pdf";
import LinearProgress from "@material-ui/core/LinearProgress";
import { uploadToIPFS } from "../blockchain/IPFSService";
import { addDegree } from "../blockchain";
import { useGlobalState } from "../store";
import { pdfjs } from "react-pdf";
pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      position: "relative",
      marginTop: "7rem",
    },
    loading: {
      position: "relative",
      margin: "2rem 20% 0 -40%",
    },
    ipfs: {
      height: "1rem",
      width: "1rem",
      marginRight: "0.5rem",
      bottom: "0.3rem",
      right: "calc(50% - 3.3rem)",
      position: "absolute",
      borderRadius: "50%",
      padding: "2px",
      border: "2px solid #44979a",
    },
    ethereum: {
      height: "1rem",
      width: "1rem",
      bottom: "0.3rem",
      right: "calc(50% - 4.5rem)",
      position: "absolute",
      borderRadius: "50%",
      padding: "2px",
      border: "2px solid #1671cc",
    },
    progress: {
      bottom: "0.9rem",
      right: "1.5rem",
      width: "calc(50% - 6.25rem)",
      position: "absolute",
      borderRadius: "1rem",
    },
    dropzone: {
      margin: "3rem auto",
      width: "60%",
      color: "#777777",
    },
    form: {
      padding: "1rem 1.5rem 1rem 6rem",
      marginTop: "-2rem",
    },
    close: {
      position: "absolute",
      top: 0,
      right: 0,
      color: "red",
      margin: "-1.5rem -1.5rem 0 0",
    },
    preview: {
      display: "inline-block",
      marginTop: "2rem",
    },
  })
);

export default function AssetUploader() {
  const classes = useStyles();
  const [uploadingStage, setUploadingStage] = useState(-1); // -1 is no-uploading, 0 is uploaded-to-ipfs, 1 is written-to-blockchain
  const [file, setFile] = useState();
  const [, setNumPages] = useState(null);
  const [pageNumber] = useState(1);

  const [metadata, setMetadata] = useState({
    studentId: "",
    studentName: "",
  });
  const [, setShowUploadDocument] = useGlobalState("showUploadDocument");

  const doUpload = async () => {
    try {
      setUploadingStage(0);
      const ipfsHash = await uploadToIPFS(file, metadata);
      setUploadingStage(1);
      await addDegree(ipfsHash, metadata.studentId, metadata.studentName);
      setUploadingStage(-1);
      setTimeout(() => setShowUploadDocument(false), 1000);
    } catch (e) {
      setUploadingStage(-1);
    }
  };

  const { studentId, studentName } = metadata;

  return (
    <div className={classes.root}>
      <form
        className={classes.form}
        onChange={({ target }) => {
          const _metadata = { ...metadata };
          _metadata[target.id] = target.value;
          setMetadata(_metadata);
        }}
      >
        <TextField
          id="studentId"
          label={"studentId"}
          value={studentId}
          style={{ width: "10rem", marginRight: "5rem" }}
          disabled={uploadingStage >= 0}
        />
        <TextField
          id="studentName"
          label={"studentName"}
          value={studentName}
          style={{ width: "20rem", marginRight: "5rem" }}
          disabled={uploadingStage >= 0}
        />
        <Button
          variant="contained"
          color="primary"
          disabled={uploadingStage >= 0 || !file || !studentName}
          style={{ verticalAlign: "bottom" }}
          onClick={doUpload}
        >
          Upload
        </Button>
      </form>
      <div className={classes.loading} style={{ opacity: uploadingStage >= 0 ? 1 : 0 }}>
        <img alt="ipfs" src={"ipfs.png"} className={classes.ipfs + (uploadingStage === 0 ? " rotate" : "")} />
        <img alt="ethereum" src={"ethereum.png"} className={classes.ethereum + (uploadingStage === 1 ? " rotate" : "")} />
        <LinearProgress className={classes.progress} color={"primary"} />
      </div>
      <div className={classes.dropzone} style={{ display: Boolean(file) ? "none" : "block", opacity: uploadingStage >= 0 ? 0.4 : 1 }}>
        <DropzoneArea
          filesLimit={1}
          acceptedFiles={["application/pdf"]}
          cancelButtonText={"cancel"}
          submitButtonText={"submit"}
          maxFileSize={50000000}
          showPreviews={false}
          showstudentIdsInPreview={false}
          showAlerts={["error", "info"]}
          onChange={(files) => setFile(files[0])}
          disabled={uploadingStage >= 0}
        />
      </div>
      {file && (
        <Paper elevation={3} className={classes.preview}>
          <Document file={file} onLoadSuccess={({ numPages }) => setNumPages(numPages)}>
            <Page pageNumber={pageNumber} />
          </Document>
        </Paper>
      )}
    </div>
  );
}
