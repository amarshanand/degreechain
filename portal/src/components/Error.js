// @ts-nocheck

import { createStyles, makeStyles } from "@material-ui/core/styles";
import { shuffle } from "../util";
import { useGlobalState } from "../store";

/**
 * A cool way of diplaying errors ... Windows style, decorated with hexspeaks (https://en.wikipedia.org/wiki/Hexspeak)
 */
const useStyles = makeStyles((theme) =>
  createStyles({
    blueScreenOfDeath: {
      padding: "2em",
      fontFamily: '"Lucida Console", Monaco, monospace',
      color: "white",
    },
    crashMessage: {
      color: "white !important",
      margin: "6rem 0 4rem 0",
    },
    cheveron: {
      fontSize: "1.5rem",
    },
    hexCodes: {
      cursor: "pointer",
      width: "60%",
    },
  })
);

export default function Error() {
  const classes = useStyles();
  const [crashMessage] = useGlobalState("crashMessage");

  document.body.style.background = "rgb(41,0,173)";
  return (
    <div className={classes.blueScreenOfDeath}>
      <div className={classes.hexCodes} onClick={() => window.open("https://en.wikipedia.org/wiki/Hexspeak", "_blank")}>
        {hexCodes}
      </div>
      <div className={classes.crashMessage} dangerouslySetInnerHTML={{ __html: `FATAL : ${crashMessage}` }} />
      C:\<span className={classes.cheveron}>›</span> <span className="blinking-cursor">█</span>
    </div>
  );
}

const hexCodes = shuffle([
  "0x000FF1CE",
  "0x00BAB10C",
  "0x8BADF00D",
  "0x1BADB002",
  "0x1CEB00DA",
  "0xB105F00D",
  "0xB16B00B5",
  "0xBAAAAAAD",
  "0xBAADF00D",
  "0xBAD22222",
  "0xBAADA555",
  "0xBADDCAFE",
  "0xC00010FF",
  "0xC0DED00D",
  "0xCAFEBABE",
  "0xCAFED00D",
  "0xCEFAEDFE",
  "0x0D15EA5E",
  "0xDABBAD00",
  "0xDEADBAAD",
  "0xDEADBABE",
  "0xDEADBEAF",
  "0xDEADC0DE",
  "0xDEADDEAD",
  "0xDEADD00D",
  "0xDEADFA11",
  "0xDEAD10CC",
  "0xDEADFEED",
  "0xDEFEC8ED",
  "0xE011CFD0",
  "0xfaceb00c",
  "0xFACEFEED",
  "0xFBADBEEF",
  "0xFEE1DEAD",
  "0xFEEDBABE",
  "0xFEEDC0DE",
  "0xFFBADD11",
  "0xD0D0CACA",
  "0x1337BABE",
]).join(" ");
