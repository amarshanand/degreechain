import { createStyles, makeStyles } from "@material-ui/core/styles";
import DegreeTable from "./DegreeTable";

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      margin: "5rem 0 0 2rem",
    },
  })
);

export default function LandingPage() {
  const classes = useStyles();

  return <DegreeTable className={classes.root} />;
}
