import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import { useGlobalState } from "../store";

/** Usage: setConfirm({ show: true, text: `Would you like to email an invoice?`, ok: doInvoice, }); */
export default function Confirm() {
  const [confirm, setConfirm] = useGlobalState("confirm");

  const onClose = () =>
    setConfirm({
      show: false,
      text: "",
      ok: () => {},
    });

  return (
    <Dialog open={confirm.show} onClose={onClose} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description">
      <DialogContent>
        <DialogContentText id="alert-dialog-description">{confirm.text}</DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose} color="primary">
          {"Cancel"}
        </Button>
        <Button
          onClick={() => {
            onClose();
            confirm.ok && confirm.ok();
          }}
          color="primary"
          autoFocus
        >
          {"Ok"}
        </Button>
      </DialogActions>
    </Dialog>
  );
}
