const express = require(`express`);
const cors = require(`cors`);
const sgMail = require(`@sendgrid/mail`);
sgMail.setApiKey(`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`);

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.post(`/api/sendmail`, function (req, res) {
  const { to, trace, file, studentName, studentId } = req.body;
  const msg = {
    to,
    from: `degreechain@gmail.com`,
    subject: `Degree certificate for ${studentName}`,
    text: `To whomesoever it may concern.\n\nDegree for ${studentName} (studentId:${studentId}) can be found at ${file}. Authenticity of thie degree can be established at ${trace}`,
    html:
      `<u><b>To whomesoever it may concern<br><br></b></u>` +
      `Degree for <b>${studentName}</b> (studentId:<b>${studentId}</b>)<br><br>` +
      `<a href='${file}'>Click here to view the degree certificate.</a><br><br>` +
      `<a href='${trace}'>Click here to verify authenticity of this degree.</a><br><br>` +
      `Regards,<br>Degreechain`,
  };
  console.log(`Sending email to ${to}`);
  sgMail
    .send(msg)
    .then((response) => {
      res.send(response);
    })
    .catch((error) => {
      console.error(error);
      res.statusCode = 500;
      res.end(error);
    });
});

app.get(`/`, (req, res) => {
  res.statusCode = 200;
  res.end(`Degreechain is up and running`);
});

const port = process.env.PORT || 3001;
app.listen(port, () => console.log(`App listening on port ${port}`));
